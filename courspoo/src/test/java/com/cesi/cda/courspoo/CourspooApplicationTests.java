package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Business.MatchBusiness;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cesi.cda.courspoo.DAO.Meet.MeetDAO;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;
import java.sql.SQLException;
import com.cesi.cda.courspoo.Controller.Match.MatchController;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;


@SpringBootTest
class CourspooApplicationTests {

	@Test
	void contextLoads() {
		assert(true==true);
	}
	// simuler réponse db
	// créer une classe mock

	@Test
	void testMeetDate() {
		try{
			Meet meet =  MeetDAO.getOne(1);
			assert(Integer.parseInt(meet.annee) < 2025);

		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	void testMeetWinner() {
		try{
			Meet meet =  MeetDAO.getOne(1);
			assert(meet.gagnant != meet.perdant);

		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	@Test
	void testCRUD(){
		try{
			MatchBusiness.create(match, 1,2);

		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}


}
