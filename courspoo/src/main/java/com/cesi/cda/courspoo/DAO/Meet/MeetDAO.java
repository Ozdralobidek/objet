package com.cesi.cda.courspoo.DAO.Meet;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;

import java.sql.*;
import java.util.ArrayList;

public class MeetDAO {
    public static ArrayList<Meet> getAll() throws SQLException {
        Connection connection = DatabaseController.getInstance();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM RENCONTRE");

        ArrayList<Meet> meets = new ArrayList<Meet>();
        while (result.next()) {
            //int id, String nom, String prenom, String anneeNaissance, String nationalite
            meets.add(new Meet(
                    result.getInt("ID"),
                    result.getInt("NUGAGNANT"),
                    result.getInt("NUPERDANT"),
                    result.getString("LIEUTOURNOI"),
                    result.getString("ANNEE")
            ));
        }
        for (Meet meet: meets) {
            System.out.println(meet.lieu);
        }
        return meets;
    }

    public static Meet getOne(int id) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM `RENCONTRE` WHERE ID=?");
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        result.first();

        Meet meet = new Meet(
                result.getInt("ID"),
                result.getInt("NUGAGNANT"),
                result.getInt("NUPERDANT"),
                result.getString("LIEUTOURNOI"),
                result.getString("ANNEE")
        );
        return meet;
    }
    public static Meet edit(Meet meet) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(

                "UPDATE RENCONTRE " +
                    "SET NUGAGNANT = ? , NUPERDANT = ?," +
                    " LIEUTOURNOI = ?, " +
                    " ANNEE = ? " +
                    "WHERE ID = ?"
        );
        statement.setInt(1, meet.gagnant);
        statement.setInt(2, meet.perdant);
        statement.setString(3, meet.lieu);
        statement.setString(4, meet.annee);
        statement.setInt(5, meet.id);
        statement.executeUpdate();

        Meet checkMeet = MeetDAO.getOne(meet.id);
        return checkMeet;
    }

    public static int delete(int id) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(
            "DELETE FROM RENCONTRE " +
                    "WHERE ID = ?"
        );
        statement.setInt(1, id);
        return statement.executeUpdate();
    }
    public static Meet getLast() throws SQLException {
        String query = "SELECT * FROM `RENCONTRE` ORDER BY `ID` DESC";
        Connection con = DatabaseController.getInstance();
        PreparedStatement stmp = con.prepareStatement(query);
        ResultSet result = stmp.executeQuery();
        result.first();
        return new Meet(
                result.getInt("id"),
                result.getInt("NUGAGNANT"),
                result.getInt("NUPERDANT"),
                result.getString("LIEUTOURNOI"),
                result.getString("ANNEE")
        );
    }
    public static int create(Meet meet) throws SQLException {
        Connection connection = DatabaseController.getInstance();
        PreparedStatement statement = connection.prepareStatement(
            "INSERT INTO RENCONTRE (`id`,`NUGAGNANT`, `NUPERDANT`, `LIEUTOURNOI`, `ANNEE`) VALUES (?, ?, ?, ?, ?)"
        );
        statement.setInt(1, meet.id);
        statement.setInt(2, meet.gagnant);
        statement.setInt(3, meet.perdant);
        statement.setString(4, meet.lieu);
        statement.setString(5, meet.annee);
        return statement.executeUpdate();
    }
}
