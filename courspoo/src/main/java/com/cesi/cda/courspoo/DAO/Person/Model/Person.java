package com.cesi.cda.courspoo.DAO.Person.Model;

public class Person {
    int id;
    String nom;
    String prenom;
    String anneeNaissance;
    String nationalite;

    public Person(int id, String nom, String prenom, String anneeNaissance, String nationalite) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.anneeNaissance = anneeNaissance;
        this.nationalite = nationalite;
    }
    public String getNomPrenom(){
        return nom + " " + prenom;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getAnneeNaissance() {
        return anneeNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAnneeNaissance(String anneeNaissance) {
        this.anneeNaissance = anneeNaissance;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }
}
