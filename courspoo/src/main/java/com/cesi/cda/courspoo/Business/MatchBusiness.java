package com.cesi.cda.courspoo.Business;

import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Meet.MeetDAO;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public abstract class MatchBusiness {
    public static List<Match> getAll() throws SQLException {
        ArrayList<Match> listeToReturn = new ArrayList<Match>();

        ArrayList<Meet> meets = MeetDAO.getAll();
        Person personne = PersonDAO.getOne(4);
        for(Meet meet : meets) {
            Person gagnant = PersonDAO.getOne(meet.gagnant);
            Person perdant = PersonDAO.getOne(meet.perdant);
            int sumAge = getAge(gagnant.getAnneeNaissance()) + getAge(perdant.getAnneeNaissance());
            listeToReturn.add(new Match(meet.id, gagnant.getNomPrenom(), perdant.getNomPrenom(), meet.lieu, meet.annee, sumAge));
        }
        return listeToReturn;
    }
    public static Match getOne(int id) throws SQLException {
        Meet meet = MeetDAO.getOne(id);
        Person gagnant = PersonDAO.getOne(meet.gagnant);
        Person perdant = PersonDAO.getOne(meet.perdant);
        int sumAge = getAge(gagnant.getAnneeNaissance()) + getAge(perdant.getAnneeNaissance());
        return new Match(meet.id, gagnant.getNomPrenom(), perdant.getNomPrenom(), meet.lieu, meet.annee, sumAge);
    }

    /**
     *
     * @param match
     * @return
     * @throws SQLException
     */
    public static Match edit(Match match) throws SQLException{
        Meet meetInit = MeetDAO.getOne(match.id);
        meetInit.lieu = match.lieu;
        meetInit.annee = match.annee;
        Meet meet = MeetDAO.edit(meetInit);
        Person gagnant = new Person(1, "il", "lo", "2020", "FR");
        Person perdant = new Person(1, "il", "lo", "2020", "FR");
        int sumAge = getAge(gagnant.getAnneeNaissance()) + getAge(perdant.getAnneeNaissance());
        return new Match(meet.id, match.getGagnant(), match.getPerdant(), meet.lieu, meet.annee, sumAge);
    }
    public static int delete(int id) throws SQLException{
        return MeetDAO.delete(id);
    }
    private static int getAge(String naissance) {
        return 2024 - Integer.valueOf(naissance);
    }
    public static int create(Match match, int idGagnant, int idPerdant) throws SQLException{

        Meet newMeet = new Meet(MeetDAO.getLast().id + 1, idGagnant, idPerdant, match.lieu, match.annee);
        return MeetDAO.create(newMeet);
    }
}
