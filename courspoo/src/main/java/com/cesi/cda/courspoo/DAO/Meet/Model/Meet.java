package com.cesi.cda.courspoo.DAO.Meet.Model;

public class Meet {
    public int id;
    public int gagnant;
    public int perdant;
    public String lieu;
    public String annee;
    public Meet(int id, int gagnant, int perdant, String lieu, String annee) {
        this.id = id;
        this.gagnant = gagnant;
        this.perdant = perdant;
        this.lieu = lieu;
        this.annee = annee;
    }


}
