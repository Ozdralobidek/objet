package com.cesi.cda.courspoo.DAO.Person;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClient;

import java.sql.SQLException;

@Repository
public class PersonDAO {
    public static Person getOne(int id) throws SQLException {
        RestClient restClient = RestClient.create();

        Person result = restClient.get()
                .uri(DatabaseController.BASE_URL +"/persons/"+ id)
                .retrieve()
                .body(Person.class);
        System.out.println(result.getNom());
        return result;
    }
}
